import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string = '';
  password: string = '';
  constructor(private modalService: NgbModal,
    private AuthService : AuthService,
    private router: Router) { }

  ngOnInit(): void {
    
  }
  login() {
   this.AuthService.login({email : this.email , password :this.password}).subscribe((response) => {
    if(response.success) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('user');
      this.AuthService.setAccessToken( response.result);
      this.router.navigate(['/dashboard']);
    }
  });
  }

}
