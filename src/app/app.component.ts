import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from './auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    // Check if the user is authenticated based on the access token
    if (!this.authService.isAuthenticated()) {
      // If not authenticated, redirect to the login page
      this.router.navigate(['/login']);
    } else {
      // If authenticated, redirect to the dashboard
      this.router.navigate(['/dashboard']);
    }
  }

}
