import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import {TasksService} from '../tasks.service';
import { Router } from '@angular/router';

export class CategoryCheckboxItem {
  constructor(public id: number, public name: string, public selected: boolean = true) {}
}
@Component({
  selector: 'create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {
  formData: FormGroup;
 
  selectedItems = [];
  categories: CategoryCheckboxItem[] = [];
  constructor(private formBuilder: FormBuilder,
    private tasksService : TasksService,
    private router: Router) {
    this.formData = this.formBuilder.group({
      description : ['', Validators.required],
      deadline : [new Date(), Validators.required],
      categories : [[], Validators.required],
      sub_tasks: this.formBuilder.array([]),

    });
   }

  ngOnInit(): void {
    this.tasksService.getCategories().subscribe((response) => {
      if(response.success) {
        
        for (const item of response.result) 
          this.categories.push(new CategoryCheckboxItem(item.id, item.name,false)) ;
        
      }
    });
 
  }
  onCheckboxChange(checkbox: CategoryCheckboxItem) {
    checkbox.selected = !checkbox.selected;
    let selected = this.categories.filter(item => item.selected === true);
    console.log(selected)
    selected.length > 0 ? this.formData.patchValue({ categories: selected.map(item => item.id)}) : this.formData.patchValue({ categories: [] }); 
  }
  get sub_tasks(): FormArray {
    return this.formData.get('sub_tasks') as FormArray;
  }
  removeInput(index: number) {
    this.sub_tasks.removeAt(index);
  }
  addSubTaskInput() {
    const newInput = new FormControl('');
    this.sub_tasks.push(newInput);
  }
  onSubmit() {
    // Handle the form submission here
    this.tasksService.createTask(this.formData.value).subscribe((response) => {
       if(response.success)
       this.router.navigate(['/tasks']);

    });
  }

}
