import { Component, OnInit } from '@angular/core';
import {TasksService} from '../tasks.service';

@Component({
  selector: 'tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})

export class TasksListComponent implements OnInit {
  public data: [];
  public headerRow =[ "ID", 'Description', 'deadline', 'Expired', 'Details'];
  constructor(private tasksService : TasksService) { 
  }

  ngOnInit(): void {
    this.tasksService.getTasks([]).subscribe((response) => {
      if(response.success) 
        this.data = response.result.data;

    });
  }

}
