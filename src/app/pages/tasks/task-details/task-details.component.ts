import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {TasksService} from '../tasks.service';


@Component({
  selector: 'task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  public isCollapsed =false;
  public item :any;
  constructor(private tasksService : TasksService,
    private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = params['id']; 
      this.tasksService.getTaskById(id).subscribe((response) => {
        if(response.success) {
          this.item = response.result;
          this.item.sub_tasks = JSON.parse(this.item.sub_tasks); 
        }
       
      });

    });
  }

}
