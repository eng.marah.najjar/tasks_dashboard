import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {Response} from '../../response.modal';

@Injectable({
  providedIn: 'root'
})

export class TasksService {

  private apiUrl: string = environment.apiUrl; 

  constructor(private http: HttpClient) {}

  getTasks(body): Observable<Response> {
    return this.http.post<Response>(`${this.apiUrl}/getTasks`,body);
  }
  createTask(body): Observable<Response> {
    return this.http.post<Response>(`${this.apiUrl}/createTask`,body);
  }
  getTaskById(id): Observable<Response> {
    return this.http.get<Response>(`${this.apiUrl}/getTaskById/`+ id);
  }
  getCategories(): Observable<Response> {
    return this.http.get<Response>(`${this.apiUrl}/getCatgeries`);
  }
  
}
