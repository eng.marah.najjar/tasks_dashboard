import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { TasksListComponent } from '../../pages/tasks/tasks-list/tasks-list.component';

import { CreateTaskComponent } from '../../pages/tasks/create-task/create-task.component';
import { TaskDetailsComponent } from '../../pages/tasks/task-details/task-details.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',        component: DashboardComponent },
    { path: 'tasks' ,           component: TasksListComponent },
    { path: 'newtask',          component: CreateTaskComponent  },
    { path: 'taskdetails/:id',  component: TaskDetailsComponent }
];
