import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http'; // Import HttpClientModule

import { AdminLayoutRoutes } from './admin-layout.routing';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardComponent }       from '../../pages/dashboard/dashboard.component';

import { TasksListComponent } from '../../pages/tasks/tasks-list/tasks-list.component';
import { CreateTaskComponent } from '../../pages/tasks/create-task/create-task.component';
import { TaskDetailsComponent } from '../../pages/tasks/task-details/task-details.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [
    DashboardComponent,
    TasksListComponent,
    CreateTaskComponent,
    TaskDetailsComponent
  ]
})

export class AdminLayoutModule {}
